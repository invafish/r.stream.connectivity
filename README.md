# r.stream.connectivity
A GRASS GIS module for analysing connectivity of freshwater networks

# Manual
https://invafish.gitlab.io/r.stream.connectivity

# Author
Stefan Blumentrath, [Norwegian Institute for Nature Research (NINA)](https://www.nina.no/), Oslo, Norway

Written for the [INVAFISH](https://prosjektbanken.forskningsradet.no/#/project/NFR/243910)
project (RCN MILJ&Oslash;FORSK grant 243910)
