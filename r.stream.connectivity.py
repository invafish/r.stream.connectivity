#!/usr/bin/env python3

"""
MODULE:       r.stream.connectivity
AUTHOR(S):    Stefan Blumentrath
PURPOSE:      Identify isolated or accessible clusters in a river network based on slope threshold
COPYRIGHT:    (C) 2019 by the GRASS Development Team
              This program is free software under the GNU General Public
              License (>=v2). Read the file COPYING that comes with GRASS
              for details.
"""

# To do:
# - add coordinates option

#%module
#% description: Identify isolated or accessible clusters in a river network based on slope threshold
#% keyword: raster
#% keyword: slope
#% keyword: stream
#% keyword: connectivity
#%end

#%option G_OPT_R_INPUTS
#% key: slope
#% label: Raster map with stream slope estimates
#% description: Input raster map with stream slope estimates (e.g. from r.stream.slope or r.slope.direction)
#% multiple: YES
#% required: YES
#%end

#%option G_OPT_R_INPUTS
#% key: direction
#% label: Raster map with flow direction
#% description: Input raster map with flow direction (e.g. from r.stream.extract)
#% required: NO
#%end

#%option G_OPT_V_INPUT
#% key: start_points
#% label: Vector points map with start locations
#% description: Input vector points map with start locations for connectivity analysis (e.g. species occurrences or river mouth)
#% required: NO
#%end

#%option G_OPT_R_INPUT
#% key: start_raster
#% label: Raster map with start locations
#% description: Input raster map with start locations for connectivity analysis (e.g. species occurrences or river mouth)
#% required: NO
#%end

#%option G_OPT_V_INPUT
#% key: barrier_points
#% label: Vector points map with barrier locations
#% description: Input vector points map with locations of barriers that limit connectivity (e.g. dams)
#% required: NO
#%end

#%option G_OPT_R_INPUT
#% key: barrier_raster
#% label: Raster map with barriers
#% description: Input raster map with locations of barriers that limit connectivity (e.g. dams)
#% required: NO
#%end

#%option G_OPT_V_FIELD
#%end

#%option G_OPT_DB_WHERE
#%end

#%option
#% key: threshold
#% label: Intraversable slope thresholds
#% description: Comma separated list of slope threshold values (e.g. confidence intervals or for different species)
#% type: string
#% multiple: YES
#% required: YES
#%end

#%option
#% key: snap
#% label: Snapping distance (in map units) occurrences and barriers (default=0)
#% description: Snapping distance (in map units) for snapping occurrences and barriers to closest stream pixel
#% type: integer
#% answer: 0
#%end

#%option G_OPT_R_OUTPUTS
#% key: clusters
#% label: Raster map with isolated clusters of the network
#% required: NO
#%end

#%option G_OPT_R_OUTPUTS
#% key: clusters_reachable
#% label: Raster map with isolated clusters of the network reachable from the start_points
#% required: NO
#%end

#%flag
#% key: a
#% label: Treat input slope as absolute values
#%end

#%flag
#% key: d
#% label: Assume downstream connectivity
#%end

#%flag
#% key: o
#% label: Include outlet coordinates as start_points
#%end

#%flag
#% key: s
#% label: Apply threshold value to each input slope map
#%end

#%rules
#% required: where,layer,start_points
#% required: clusters,clusters_reachable
#% requires: clusters_reachable,start_points,direction
#% requires: -d,start_points
#% requires_all: -d,clusters_reachable,direction
#% requires_all: -o,clusters_reachable,direction
#% exclusive: barrier_points,barrier_raster
#% exclusive: start_points,start_raster
#%end

# toDo:
# - write tests
# - reconsider if s_flag is useful
# - implement snapping to closest pixel on stream (in number of pixels or mapunits???)
# - Add barrier layer option (dams, wires, ...)
# - issue warning if occurrences fall on barriers (possibly add option to print conflicting locations)
# - issue warning if occurrences or barriers do not fall on stream pixels (possibly add option to print locations / categories of disjoint barriers/occurrences)
# - implement barriers/occurrences as vector points map

# if start_points:
#     gcript.run_command('v.to.rast', )


import atexit
import os
from io import BytesIO

import numpy as np
import grass.script as gscript


def cleanup():
    """Remove temporary maps"""
    nuldev = open(os.devnull, 'w')
    gscript.run_command('g.remove', flags='bf', quiet=True,
                        type=['raster'], stderr=nuldev,
                        name=tmp_maps)


def get_outlets(direction):
    """Extract outlet raster points from stream flow direction map"""
    outlet_map = '{}_outlet'.format(tmpname)
    mc_expression = """{out}=if((abs({dir})==1&&isnull({dir}[-1,1])) || \
(abs({dir})==2&&isnull({dir}[-1,0])) || \
(abs({dir})==3&&isnull({dir}[-1,-1])) || \
(abs({dir})==4&&isnull({dir}[0,-1])) || \
(abs({dir})==5&&isnull({dir}[1,-1])) || \
(abs({dir})==6&&isnull({dir}[1,0])) || \
(abs({dir})==7&&isnull({dir}[1,1])) || \
(abs({dir})==8&&isnull({dir}[0,1])), \
1,null())""".format(out=outlet_map, dir=direction)
    gscript.run_command('r.mapcalc', expression=mc_expression, quiet=True, overwrite=True)
    tmp_maps.append(outlet_map)
    return outlet_map


def stdout2numpy(stdout=None, sep=',', names=None):
    """Read table like output from grass modules as Numpy array (Python3 only)"""
    if type(stdout) == str:
        stdout = gscript.encode(stdout)
    elif type(stdout) != byte:
        gscript.fatal(_('Unsupported data type'))
    np_array = np.genfromtxt(BytesIO(stdout), dtype=None, delimiter=sep, names=names)
    return np_array


def get_coords(raster, offset=0, snap=0):
    """Extract coordinates from raster map and assign ID (with possible offset)"""
    # Snapping only needed for:
    # - start_points / start_raster
    # - barrier_points / barrier_raster

    if snap > 0:
        coords = stdout2numpy(gscript.read_command('r.stats', flags='gn', quiet=True,
                                                   input=raster, separator=',').rstrip('\n'),
                              names=['x', 'y', 'value'])
        coords[raster] = np.arange(offset + 1, offset + 1 + len(coords['x']), 1)
    else:
        distance = '{}_distance'.format(tmpname)
        distance_rc = '{}_distance_rc'.format(tmpname)
        value = '{}_value'.format(tmpname)
        gscript.run_command('r.grow.distance', input=raster, distance=distance, value=value,
                            metric='squared', overwrite=True, verbose=True)
        gscript.write_command('r.reclass', input=distance, output=distance_rc, rules='-',
                              stdin='0 thru {} = 1\n* = NULL'.format(snap**2),
                              overwrite=True, verbose=True)
        coords = stdout2numpy(gscript.read_command('r.stats', flags='gn', quiet=True,
                                                   input=[value, slope, distance, distance_rc], separator=',').rstrip('\n'),
                              names=['x', 'y'] + [raster, slope, distance, distance_rc])

        # Get sorted "rows"
        coords.sort(order=[raster, distance, slope], axis=0)
        i = np.nonzero(np.diff(coords[raster]))[0] + 1
        coords = coords[np.append(0, i)]
    return coords


"""
options = {
'slope': 'SLOPE_11',
'direction': 'DIR',
'start_points': 'RANDOM',
'where': None,
'layer': '1',
'threshold': '150',
'clusters': 'clusters',
'clusters_reachable': 'clusters_reachable'
}

# Flags are probably not needed (if )
flags = {'a': True,
         'd': True,
         'o': True,
         's': False}
"""


def main():
    """Do the main work"""

    # Define static/global variables
    global tmp_maps
    tmp_maps = []
    global tmpname
    tmpname = gscript.tempname(12)

    # Define user input variables
    # Flags
    a_flag = flags['a']
    d_flag = flags['d']
    o_flag = flags['o']
    s_flag = flags['s']

    # Options
    slope = options['slope'].split(',')
    direction = options['direction']
    start_points = options['start_points']
    layer = options['layer']
    where = options['where']

    try:
        thresholds = [float(x) for x in options['threshold'].split(',')]
    except:
        gscript.fatal(_('Not all thresholds given as numeric value'))

    if options['clusters']:
        clusters = options['clusters'].split(',')
    else:
        clusters = '{}_clust'.format(tmpname)

    if options['clusters_reachable']:
        clusters_reachable = options['clusters_reachable'].split('\n')

    # More static variables
    current_region = gscript.region()
    nsres, ewres = current_region['nsres'], current_region['ewres']

    # Check if variables are set properly
    if options['clusters'] and len(clusters) != len(thresholds):
        gscript.fatal(_('Number of thresholds and requested output differs'))

    if options['clusters'] and len(clusters) != len(thresholds):
        gscript.fatal(_('Number of thresholds and requested output differs'))

    if options['clusters_reachable'] and len(clusters_reachable) != len(thresholds):
        gscript.fatal(_('Number of thresholds and requested output differs'))

    if s_flag and len(slope) == 1:
        gscript.warning(_('Only one slope input map provided. s-flag will be ignored'))

    if len(slope) > 1 and len(slope) != len(thresholds):
        gscript.fatal(_('If multiple slope maps are given, their number has to match with provided number of threshold values'))

    fragments = '{}_fragments'.format(tmpname)
    rc_map = '{}_reclass'.format(tmpname)
    tmp_maps.append(fragments)
    tmp_maps.append(rc_map)

    if d_flag:
        # Routing occurrences downstreams if requested
        path_map = '{}_downpath'.format(tmpname)
        tmp_maps.append(path_map)
        kwargs = {
            'input': direction,
            'raster_path': path_map,
            'start_points': start_points,
            'quiet': True,
            'overwrite': True
            }

        gscript.run_command('r.path', **kwargs)
        downstream = ' || ! isnull({})'.format(path_map)
    else:
        downstream = ''

    if options['clusters_reachable']:
        if start_points:
            coords = stdout2numpy(gscript.read_command('v.out.ascii', input=start_points,
                                                       format='point', layer=layer, where=where,
                                                       separator=',', quiet=True).rstrip('\n'), sep=',')

        if o_flag or not start_points:
            outlet_map = get_outlets(direction)
            if start_points:
                offset = max(coords[:, 2])
                coords = np.append(coords, get_coords(outlet_map, offset), axis=0)
            else:
                gscript.verbose('No start points given. Starting from outlet points...')
                coords = get_coords(outlet_map, 0)


    for threshold in thresholds:
        idx = thresholds.index(threshold)

        # Exclude pixels above slope threshold from river network
        mc_expresion = '{fragments}=if(\
{abs}({slope}) < {threshold}{downstream}, \
1, null())'.format(fragments=fragments,
                   downstream=downstream,
                   slope=slope[idx],
                   threshold=threshold,
                   resolution=(nsres+ewres)/2,
                   abs='abs' if a_flag else '')
        gscript.run_command('r.mapcalc', expression=mc_expresion,
                            overwrite=True, quiet=True)


        # Identify isolated clusters of the river network
        gscript.run_command('r.clump', flags='d', input=fragments,
                            output=clusters[idx],
                            overwrite=True, quiet=True)

        # Write raster history
        if options['clusters']:
            gscript.raster_history(clusters[idx])


            clumps = np.unique(stdout2numpy(gscript.read_command('r.what', flags='c',
                                                                 map=clusters[idx], coordinates=coords[:, :2],
                                                                 separator=',', null_value=0).replace(',,', ',').rstrip('\n'))[:, 2])

            rules = '\n'.join('{0} = {0}'.format(y) for y in clumps)
            rules += "\n* = NULL"

            gscript.write_command('r.reclass', input=clusters[idx], rules='-',
                                  output=rc_map, stdin=rules, overwrite=True, quiet=True)

            # Make output map permanent
            gscript.run_command('r.mapcalc', expression='{}={}'.format(clusters_reachable[idx],
                                                                       rc_map), overwrite=True, quiet=True)

            # Write raster history
            gscript.raster_history(clusters_reachable[idx])

if __name__ == "__main__":
    options, flags = gscript.parser()
    atexit.register(cleanup)
